import Warrior from './warrior.js'
import WarriorSword from './warriorSword.js'
import WarriorSpear from './warriorSpear.js'

class WarriorAxe extends Warrior {

    attack(opponent) {
        if (opponent instanceof WarriorSword) {
            opponent.life -= this.power*2
            if (opponent.life < 0) {
                opponent.life = 0
            }
        } else {
            super.attack(opponent)
        }
        return opponent.life
    }
}

export default WarriorAxe;