import Warrior from './warrior.js'
import WarriorAxe from './warriorAxe.js'
import WarriorSword from './warriorSword.js'
import WarriorSpear from './warriorSpear.js'

const constantin = new Warrior("constantin", 12, 52)
const josuette = new Warrior("josuette", 16, 42)

const milouz = new WarriorAxe("milouz", 15, 61)

const pinpon = new WarriorSpear("pinpon", 11, 53)

const jeanTasse = new WarriorSword("jeanTasse", 16, 48)

let message = ""
const fight = (warrior1, warrior2) => {
    for (let i = 1; warrior1.life > 0 && warrior2.life > 0; i++) {
        console.log("ROUND " + i)
        warrior1.attack(warrior2)
        console.log(warrior2.name, warrior2.life)
        warrior2.attack(warrior1)
        console.log(warrior1.name, warrior1.life)
        if (warrior1.life == 0 && warrior2.life == 0) {
            message = "It's a draw."
        } else if (warrior1.life <= 0) {
            message = warrior2.name + " wins."
        } else {
            message = warrior1.name + " wins."
        }
    }
    return message
}

// TEST attack() & isAlive() 

// console.log(josuette)
// console.log(jeanTasse)
// console.log("jeanTasse.power", jeanTasse.power)
// console.log('josuette.life', jeanTasse.attack(josuette))

// josuette.attack(constantin)
// console.log('1', constantin.isAlive())
// console.log('1', constantin)
// console.log('1', josuette)

// josuette.attack(constantin)
// console.log('2', constantin.isAlive())
// console.log('2', constantin)
// console.log('2', josuette)

// josuette.attack(constantin)
// console.log('3', constantin.isAlive())
// console.log('3', constantin)
// console.log('3', josuette)

// josuette.attack(constantin)
// console.log('4', constantin.isAlive())
// console.log('4', constantin)
// console.log('4', josuette)

// TEST attack() Sword > Spear 

// console.log(jeanTasse)
// console.log(jeanTasse instanceof WarriorSword)
// console.log(pinpon)
// console.log(pinpon instanceof WarriorSpear)
// console.log(jeanTasse.attack(pinpon))
// console.log(jeanTasse.attack(pinpon))

// TEST attack() Spear > Axe

// console.log(pinpon)
// console.log(pinpon instanceof WarriorSpear)
// console.log(milouz)
// console.log(milouz instanceof WarriorAxe)
// console.log(pinpon.attack(milouz))
// console.log(pinpon.attack(milouz))
// console.log(pinpon.attack(milouz))

// TEST fight

// console.log(fight(jeanTasse, constantin))