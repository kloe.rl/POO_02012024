# Compétences testées :

- Connaissance des concepts de la programmation orientée objet
- Les bonnes pratiques de la programmation orientée objet (POO) sont respectées
- Coder dans un langage orienté objet avec un style défensif
- Le code source est documenté
- Utiliser les normes de codage du langage
- Utiliser un outil de gestion de versions
