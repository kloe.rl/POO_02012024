class Warrior {
    constructor(name, power, life) {
        this.name = name,
        this.power = power,
        this.life = life
    }

    attack(opponent) {
        opponent.life -= this.power 
        if (opponent.life < 0) {
            opponent.life = 0
        }
        return opponent.life
    }
    
    isAlive() {
        return this.life > 0 ? true : false;
    }
}

export default Warrior;