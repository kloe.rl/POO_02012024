import Warrior from './warrior.js'
import WarriorAxe from './warriorAxe.js'
import WarriorSpear from './warriorSpear.js'

class WarriorSword extends Warrior {

    attack(opponent) {
        if (opponent instanceof WarriorSpear) {
            opponent.life -= this.power*2
            if (opponent.life < 0) {
                opponent.life = 0
            }
        } else {
            super.attack(opponent)
        }
        return opponent.life
    }
}

export default WarriorSword;